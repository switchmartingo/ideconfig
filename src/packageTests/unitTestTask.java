package packageTests;

import org.junit.Test;
import sourceCode.Collaborator;
import sourceCode.Task;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class unitTestTask {

	@Test
	public void test_US205_isDone1() {
		String description = "gerir tarefas";
		String dueDate = "17/11/2018";
		String startDate = "15/11/2018";
		Task work = new Task(description, dueDate, startDate);

		work.isDone();
		assertTrue(work.getIsDone());
	}

	@Test
	public void test_US205_isDone2() {
		String description = "gerir tarefas";
		String dueDate = "17/11/2018";
		String startDate = "15/11/2018";
		Task work = new Task(description, dueDate, startDate);

		assertFalse(work.getIsDone());
	}

	@Test
	public void test_US203_taskListAllCompleted() {
		/**
		 * List with only 1 task (with isDone == true and dueDate > today) must result
		 * in empty toDoList
		 */
		String description = "Come�ar a trabalhar";
	}

	@Test
	public void test_US203_emptyTaskList() {
		// Lista de tarefas vazia tem de resultar em lista pendentes vazia

		ArrayList<Task> expecteds = new ArrayList<Task>();
		Collaborator A = new Collaborator();
		ArrayList<Task> actuals = A.toDoList();
		assertEquals(expecteds, actuals);

	}

	/**
	 * Test to the "equals" method of class Task. Equals is overriding Object method
	 * with same name.
	 */
	@Test
	public void test_tasksEqualityShouldBeOK() {
		String description1 = "gerir tarefas";
		String dueDate1 = "17/11/2018";
		String startDate1 = "15/11/2018";

		Task task1 = new Task(description1, dueDate1, startDate1);

		String description2 = "gerir tarefas";
		String dueDate2 = "17/11/2018";
		String startDate2 = "15/12/2018";

		Task task2 = new Task(description2, dueDate2, startDate2);

		assertEquals(task1, task2);

		//ASAS

		String description3 = "gerir tarefas";
		String dueDate3 = "17/12/2018";
		String startDate3 = "15/12/2018";

		Task task3 = new Task(description3, dueDate3, startDate3);

		assertFalse(task1.equals(task3));

		String description4 = "gerire tarefas";
		String dueDate4 = "17/11/2018";
		String startDate4 = "15/12/2018";

		Task task4 = new Task(description4, dueDate4, startDate4);

		assertFalse(task1.equals(task3));
	}

	public void testRemoveTask() {
		// Collaborator instance = new
		// Collaborator("John","test@gmail.com","225111222","911222333","1985/01/01");

	}

	public int test() {
		return 0;
	}

	public void teste222() {
		int a = 9;
	}

}
